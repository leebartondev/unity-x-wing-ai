﻿using UnityEngine.SceneManagement;
using UnityEngine;

/// ////////////////////////////////////////////////////////
// S H I P S   S E L E C T E D   M E N U
/// ////////////////////////////////////////////////////////

public class ShipsSelectedMenu : SelectionMenu {

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    #pragma warning disable 0649
    [SerializeField] private GameObject manageButton;
    [SerializeField] private GameObject startButton;

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    private bool manageButtonClicked = false;

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ///////////////////////////////////////////////////////

    // Determine if the ships selected menu should be displayed or not.
    protected override void ShowOrHideMenu () {
        this.ShouldShowManageButton();
        this.ShouldShowStartButton();
    }

    // Determine if the manage ships button should be displayed.
    private void ShouldShowManageButton () {
        if (this.shipSelectionManager.GetSelectedShipsCount() > 0 && !this.shipSelectionManager.GetHasSelectedFaction()) {
            this.ShowManageButton();
        } else {
            this.HideManageButton();
        }
    }

    // Determine if the start ai button should be displayed.
    private void ShouldShowStartButton () {
        if (this.shipSelectionManager.GetSelectedShipsCount() > 0 && !this.shipSelectionManager.GetHasSelectedFaction()) {
            this.ShowStartButton();
        } else {
            this.HideStartButton();
        }
    }

    // Show management button.
    private void ShowManageButton () {
        if (!this.manageButtonClicked) {
            this.manageButton.SetActive(true);
        } else {
            this.manageButtonClicked = false;
        } 
    }

    // Hide management button.
    private void HideManageButton () {
        this.manageButton.SetActive(false);
    }

    // Show start button.
    private void ShowStartButton () {
        this.startButton.SetActive(true);
    }

    // Hide start button.
    private void HideStartButton () {
        this.startButton.SetActive(false);
    }

    // Begin AI on start button click.
    public void StartAI () {
        string aiType = PlayerPrefs.GetString(SaveKeys.SCENE_TO_LOAD);
        Debug.Log(aiType);
        if (aiType != null) {
            SceneManager.LoadScene(aiType);
        } else {
            Debug.Log(SceneNames.CLASS_AI);
            SceneManager.LoadScene(SceneNames.CLASS_AI);
        }
    }

    // Show AI ship management menu.
    public void ManageButtonClick () {
        this.manageButtonClicked = true;
        this.Show();
        this.shipSelectionManager.SetIsManagingShips(true);
        this.HideManageButton();
    }

    // Hide AI ship management menu.
    public void CloseButtonClick () {
        this.Hide();
        this.shipSelectionManager.SetIsManagingShips(false);
        this.ShouldShowManageButton();
    }

}
