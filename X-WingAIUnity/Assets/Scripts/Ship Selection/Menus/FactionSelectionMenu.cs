﻿/// ////////////////////////////////////////////////////////
// F A C T I O N   S E L E C T I O N   M E N U
/// ////////////////////////////////////////////////////////
using UnityEngine;
public class FactionSelectionMenu : SelectionMenu {

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Determine if the ship selection menu should be displayed or not.
    protected override void ShowOrHideMenu () {
        if (this.shipSelectionManager.GetHasSelectedFaction() || this.shipSelectionManager.GetIsManagingShips()) {
            this.Hide();
        } else {
            this.Show();
        }
    }
}
