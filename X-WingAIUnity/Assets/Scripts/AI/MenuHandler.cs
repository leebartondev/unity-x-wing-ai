﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuHandler : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    [Header("Forward Maneuvers")]
    public RawImage blueForwardManeuver; // Forward
    public RawImage whiteForwardManeuver;
    public RawImage redForwardManeuver;

    [Header("Bank Maneuvers")]
    public RawImage blueBankLeftManeuver; // Bank
    public RawImage whiteBankLeftManeuver;
    public RawImage blueBankRightManeuver;
    public RawImage whiteBankRightManeuver;

    [Header("Turn Maneuvers")]
    public RawImage blueTurnLeftManeuver; // Turn
    public RawImage whiteTurnLeftManeuver;
    public RawImage blueTurnRightManeuver;
    public RawImage whiteTurnRightManeuver;

    [Header("K-Turn Maneuvers")]
    public RawImage redKTurnManeuver; // K Turn

    // Factions
    [Header("AI Faction Logos")]
    public RawImage scumLogo;
    public RawImage rebelLogo;
    public RawImage empireLogo;
    public RawImage resistanceLogo;
    public RawImage firstOrderLogo;
    public RawImage republicLogo;
    public RawImage seperatistLogo;

    // Ship Actions / Tokens
    [Header("AI Actions")]
    public RawImage targetLockActionImage;
    public RawImage focusActionImage;
    public RawImage barrelRollActionImage;
    public RawImage barrelRollRedActionImage;

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    // Class Objects
    private TargetingSystem targetingSystem;
    private Ship ship;

    // Menu GameObjects
    private GameObject activityMenu;
    private GameObject resultsMenu;

    // Ship Menu GameObjects
    private GameObject shipMenuFaction;
    private GameObject shipMenuActions;
    private GameObject shipMenuActionsFree;
    private GameObject shipMenuActionsAlways;
    private GameObject shipMenuActionsAttack;
    private GameObject shipMenuActionsEvade;

    // AI Faction Image
    private RawImage factionLogo;

    // AI Action Images
    private RawImage freeActionImage;
    private RawImage alwaysActionImage;
    private RawImage attackActionImage;
    private RawImage evadeActionImage;

    // Maneuver Image & Distance
    private RawImage currentDisplayedManeuver;
    private TextMeshProUGUI distance;

    /// ////////////////////////////////////////////////////////
    // U I   H A N D L E R
    /// ////////////////////////////////////////////////////////

    void Start() {
        ship = GameObject.FindGameObjectWithTag("Ship").GetComponent<Ship>();
        targetingSystem = GameObject.FindGameObjectWithTag("Ship").GetComponent<TargetingSystem>();

        activityMenu = GameObject.FindGameObjectWithTag("ActivityMenu");
        HideTargetActivityMenu();

        resultsMenu = GameObject.FindGameObjectWithTag("ResultsMenu");
        HideResultsMenu();

        shipMenuFaction = GameObject.FindGameObjectWithTag("ShipMenuFaction");
        ShowShipFactionLogo();

        shipMenuActions = GameObject.FindGameObjectWithTag("ShipMenuActions");
        shipMenuActionsFree = GameObject.FindGameObjectWithTag("ShipMenuActionsFree");
        shipMenuActionsAlways = GameObject.FindGameObjectWithTag("ShipMenuActionsAlways");
        shipMenuActionsAttack = GameObject.FindGameObjectWithTag("ShipMenuActionsAttack");
        shipMenuActionsEvade = GameObject.FindGameObjectWithTag("ShipMenuActionsEvade");
        SetShipActionIcons();
        HideShipMenuActions();
    }

    // Set the selected state of activity.
    public void SetTargetingSystemActivity (int activity) {
        Debug.Log((Activity)activity);
        targetingSystem.SetTargetActivity((Activity)activity);
    }

    // Display target activity menu.
    public void ShowTargetActivityMenu () {
        activityMenu.SetActive(true);
    }

    // Hide target activity menu.
    public void HideTargetActivityMenu () {
        activityMenu.SetActive(false);
    }

    // Display maneuver results menu.
    public void ShowResultsMenu((Maneuver Maneuver, int Distance, int Difficulty) maneuver) {
        distance = resultsMenu.GetComponentInChildren<TextMeshProUGUI>();
        distance.text = maneuver.Distance.ToString();
        // Check what maneuver image to display
        SetCurrentDisplayedManeuver(maneuver);
        // Display decided maneuver
        DisplayCurrentManeuver();
        // Show menu
        resultsMenu.SetActive(true);
    }

    // Hide maneuver results menu.
    public void HideResultsMenu () {
        resultsMenu.SetActive(false);
    }

    // Display the faction associated with the ship.
    public void ShowShipFactionLogo () {
        SetFactionLogo(ship.faction);
        DisplayFactionLogo();
    }

    // Display AI actions to use.
    public void ShowShipMenuActions () {
        shipMenuActions.SetActive(true);
    }

    // Set the RawImages for AI actions.
    public void SetShipActionIcons () {
        InstantiateActionImage(ActionType.FREE, ref freeActionImage, shipMenuActionsFree);
        InstantiateActionImage(ActionType.ALWAYS, ref alwaysActionImage, shipMenuActionsAlways);
        InstantiateActionImage(ActionType.ATTACK, ref attackActionImage, shipMenuActionsAttack);
        InstantiateActionImage(ActionType.EVADE, ref evadeActionImage, shipMenuActionsEvade);
    }

    // Hide AI actions to use.
    public void HideShipMenuActions () {
        shipMenuActions.SetActive(false);
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////
    
    // Create and set the parent for AI ship's free action image.
    private void InstantiateActionImage (ActionType actionType, ref RawImage actionImage, GameObject parent) {
        if (actionImage != null) { Destroy(actionImage.gameObject); }
        // Get action index
        int index = 0;
        for (int i = 0; i < ship.actionTypes.Length; i++) {
            if (actionType == ActionType.ATTACK && ship.actionTypes[i] == ActionType.ATTACK_EVADE) {
                index = i;
            } else if (actionType == ActionType.EVADE && ship.actionTypes[i] == ActionType.ATTACK_EVADE) {
                index = i;
            } else {
                if (ship.actionTypes[i] == actionType)
                index = i;
            }
        }
        // Get image to display
        Action action = ship.actions[index];
        if (action == Action.TARGET_LOCK) {
            actionImage = Instantiate(targetLockActionImage);
        } else if (action == Action.FOCUS) {
            actionImage = Instantiate(focusActionImage);
        } else if (action == Action.EVADE) {
            actionImage = Instantiate(evadeActionImage);
        } else if (action == Action.BARREL_ROLL) {
            actionImage = Instantiate(barrelRollActionImage);
        } else if (action == Action.BARREL_ROLL_RED) {
            actionImage = Instantiate(barrelRollRedActionImage);
        } else {
            Debug.LogError("Invalid AI Ship Action - Setting " + actionType + " action to focus!");
            actionImage = Instantiate(focusActionImage);
        }
        // Set parent
        actionImage.transform.SetParent(parent.transform);
        actionImage.rectTransform.localPosition = new Vector3(0f, 0f, 0f);
    }

    // Determine what faction logo to display.
    private void SetFactionLogo (Faction faction) {
        if (factionLogo != null) { Destroy(factionLogo.gameObject); }
        switch (faction) {
            case Faction.REBEL: {
                factionLogo = rebelLogo;
                break;
            }
            case Faction.EMPIRE: {
                factionLogo = empireLogo;
                break;
            }
            case Faction.SCUM: {
                factionLogo = scumLogo;
                break;
            }
            case Faction.RESISTANCE: {
                factionLogo = resistanceLogo;
                break;
            }
            case Faction.FIRST_ORDER: {
                factionLogo = firstOrderLogo;
                break;
            }
            case Faction.REPUBLIC: {
                factionLogo = republicLogo;
                break;
            }
            case Faction.SEPERATIST: {
                factionLogo = seperatistLogo;
                break;
            }
            default:
                Debug.LogError("Invalid Ship Faction - Setting rebel logo as default!");
                factionLogo = rebelLogo;
                break;
        }
    }

    // Instantiate faction logo.
    private void DisplayFactionLogo () {
        factionLogo = Instantiate(factionLogo);
        factionLogo.transform.SetParent(shipMenuFaction.transform);
        factionLogo.rectTransform.localPosition = new Vector3(0f, 0f, 0f);
    }

    // Determine what maneuver to display.
    private void SetCurrentDisplayedManeuver ((Maneuver Maneuver, int Distance, int Difficulty) maneuver) {
        if (currentDisplayedManeuver != null) { Destroy(currentDisplayedManeuver.gameObject); }
        if (maneuver.Difficulty == 0) { // Blue
            if (Maneuver.FORWARD == maneuver.Maneuver) {
                currentDisplayedManeuver = blueForwardManeuver;
            }
            else if (Maneuver.BANK_LEFT == maneuver.Maneuver) {
                currentDisplayedManeuver = blueBankLeftManeuver;
            }
            else if (Maneuver.BANK_RIGHT == maneuver.Maneuver) {
                currentDisplayedManeuver = blueBankRightManeuver;
            }
            else if (Maneuver.TURN_LEFT == maneuver.Maneuver) {
                currentDisplayedManeuver = blueTurnLeftManeuver;
            }
            else if (Maneuver.TURN_RIGHT == maneuver.Maneuver) {
                currentDisplayedManeuver = blueTurnRightManeuver;
            }
        }
        else if (maneuver.Difficulty == 1) { // White
            if (Maneuver.FORWARD == maneuver.Maneuver) {
                currentDisplayedManeuver = whiteForwardManeuver;
            }
            else if (Maneuver.BANK_LEFT == maneuver.Maneuver) {
                currentDisplayedManeuver = whiteBankLeftManeuver;
            }
            else if (Maneuver.BANK_RIGHT == maneuver.Maneuver) {
                currentDisplayedManeuver = whiteBankRightManeuver;
            }
            else if (Maneuver.TURN_LEFT == maneuver.Maneuver) {
                currentDisplayedManeuver = whiteTurnLeftManeuver;
            }
            else if (Maneuver.TURN_RIGHT == maneuver.Maneuver) {
                currentDisplayedManeuver = whiteTurnRightManeuver;
            }
        }
        else if (maneuver.Difficulty == 2) { // Red
            if (Maneuver.FORWARD == maneuver.Maneuver) {
                currentDisplayedManeuver = redForwardManeuver;
            }
            else if (Maneuver.K_TURN == maneuver.Maneuver) {
                currentDisplayedManeuver = redKTurnManeuver;
            }
        }
    }

    // Instantiate currently AI selected maneuver
    private void DisplayCurrentManeuver () {
        currentDisplayedManeuver = Instantiate(currentDisplayedManeuver);
        currentDisplayedManeuver.transform.SetParent(resultsMenu.transform);
        currentDisplayedManeuver.rectTransform.localPosition = new Vector3(0f, 0f, 0f);
    }
}
