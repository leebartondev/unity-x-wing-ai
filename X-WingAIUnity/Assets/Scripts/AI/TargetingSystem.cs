﻿using UnityEngine;
using UnityEngine.UI;

public class TargetingSystem : MonoBehaviour {

    /// ////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    private static float OFFSET = 50.0f;
    private static float SCALE_X = 0.20f;
    private static float SCALE_Y = 0.20f;
    private static float SCALE_Z = 1.0f;

    /// ////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    // UI Images
    public RawImage directionImage;
    public RawImage directionImageHover;

    /// ////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /// ////////////////////////////////////////////////////////

    // System
    private Direction direction; // Direction of target selected
    private Activity activity;   // Current state of game activity
    private (Maneuver Maneuver, int Distance, int Difficulty) maneuver; // Decided maneuver

    // Ship
    private Ship ship;

    // UI Images
    private RawImage directionHoverImage;
    private RawImage directionSelectedImage;

    // UI Menus
    private MenuHandler menuHandler;

    private bool hovering;

    /// ////////////////////////////////////////////////////////
    // T A R G E T I N G   S Y S T E M
    /// ////////////////////////////////////////////////////////

    void Start() {
        ship = gameObject.GetComponent<Ship>();
        menuHandler = GameObject.FindGameObjectWithTag("MenuHandler").GetComponent<MenuHandler>();
    }

    // Determine best move for AI based on target direction.
    public void SetTargetDirection (int dir) {
        if (directionSelectedImage != null) { Destroy(directionSelectedImage.gameObject);  }
        DisplayNewImage(directionHoverImage, ref directionSelectedImage, directionImage, false);
        direction = (Direction)dir;
        menuHandler.ShowTargetActivityMenu();
        menuHandler.HideResultsMenu();
    }

    // Set the activity state and make the AI's maneuver
    // decision.
    public void SetTargetActivity (Activity activity) {
        this.activity = activity;
        menuHandler.HideTargetActivityMenu();
        menuHandler.ShowShipMenuActions();
        MakeManeuver();
    }

    // Make the AI's maneuver decision based on direction
    private void MakeManeuver () {
        maneuver = ship.GetShipManeuver(direction, activity);
        menuHandler.ShowResultsMenu(maneuver);
    }

    /// ////////////////////////////////////////////////////////
    // T A R G E T I N G   S Y S T E M   U I
    /// ////////////////////////////////////////////////////////
    
    // Move targeting arrows on hover.
    public void ShowHover (RawImage origin) {
        if (!hovering) {
            hovering = true;
            DisplayNewImage(origin, ref directionHoverImage, directionImageHover, true);
        }  
    }

    // Move targeting arrows back when not hover.
    public void HideHover () {
        Destroy(directionHoverImage.gameObject);
        hovering = false;
    }

    /// ////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /// ////////////////////////////////////////////////////////

    // Instantiate a new image based on an existing image.
    private void DisplayNewImage(RawImage fromImage, ref RawImage newImage, RawImage instantiateImage, bool offset) {
        newImage = Instantiate(instantiateImage);
        newImage.rectTransform.SetParent(fromImage.rectTransform.parent);
        newImage.rectTransform.eulerAngles = CreateNewLocalRotationFromRawImage(fromImage);
        newImage.rectTransform.localPosition = CreateNewLocalPositionFromRawImage(fromImage, offset);
        newImage.rectTransform.localScale = new Vector3(SCALE_X, SCALE_Y, SCALE_Z);
    }

    // Set local rotation based on another raw image's current
    // local rotation.
    private Vector3 CreateNewLocalRotationFromRawImage (RawImage origin) {
        return new Vector3 (
            origin.rectTransform.eulerAngles.x,
            origin.rectTransform.eulerAngles.y,
            origin.rectTransform.eulerAngles.z
        );
    }

    // Set local position based on another raw image's current
    // local position.
    private Vector3 CreateNewLocalPositionFromRawImage (RawImage origin, bool offset) {
        Vector3 originLocalPosition = origin.rectTransform.localPosition;

        // Determine x and/or y offset
        float xOffset = 0;
        float yOffset = 0;
        if (offset) {
            if (originLocalPosition.x == 0) { // Y only offset
                yOffset = GetOffsetSign(originLocalPosition.y);
            }
            else if (originLocalPosition.y == 0) { // X only offset
                xOffset = GetOffsetSign(originLocalPosition.x);
            }
            else { // X & Y offset
                xOffset = GetOffsetSign(originLocalPosition.x);
                yOffset = GetOffsetSign(originLocalPosition.y);
            }
        }

        return new Vector3(
            originLocalPosition.x + xOffset,
            originLocalPosition.y + yOffset,
            originLocalPosition.z
        );
    }

    // Determine positive or negative X offset.
    private float GetOffsetSign (float value) {
        if (value > 0) {
            return OFFSET;
        } else {
            return (OFFSET * -1.0f);
        }
    }
}
