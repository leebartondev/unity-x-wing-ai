﻿/// ////////////////////////////////////////////////////////
// V T - 4 9   D E C I M A T O R   ( S C U M )
/// ////////////////////////////////////////////////////////

public class EmpireDecimator : Ship
{
    // Used as constructor
    protected override void Start() {
        base.Start();

        // Maneuver dial
        // (Maneuver, Distance, Difficulty)
        maneuvers = new (Maneuver, int, int)[3][] {
            new (Maneuver, int, int)[] { // Blue
                (Maneuver.FORWARD, 1, 0),
                (Maneuver.FORWARD, 2, 0),
                (Maneuver.BANK_LEFT, 1, 0),
                (Maneuver.BANK_RIGHT, 1, 0),
            },
            new (Maneuver, int, int)[] { // White
                (Maneuver.FORWARD, 3, 1),
                (Maneuver.FORWARD, 4, 1),
                (Maneuver.BANK_LEFT, 2, 1),
                (Maneuver.BANK_LEFT, 3, 1),
                (Maneuver.BANK_RIGHT, 2, 1),
                (Maneuver.BANK_RIGHT, 3, 1),
                (Maneuver.TURN_LEFT, 2, 1),
                (Maneuver.TURN_LEFT, 3, 1),
                (Maneuver.TURN_RIGHT, 2, 1),
                (Maneuver.TURN_RIGHT, 3, 1),
            },
            new (Maneuver, int, int)[] { // Red
                (Maneuver.TURN_LEFT, 1, 2),
                (Maneuver.TURN_RIGHT, 1, 2),

            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   C L O S I N G
        /// ////////////////////////////////////////////////////////

        // Target in range and facing AI
        this.maneuversWhenTargetIsClosing = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   R E T R E A T I N G
        /// ////////////////////////////////////////////////////////

        // Target is in range and facing away from AI
        this.maneuversWhenTargetIsRetreating = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // T A R G E T   O U T   O F   R A N G E
        /// ////////////////////////////////////////////////////////

        // Target is not in range
        this.maneuversWhenTargetIsOutOfRange = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };

        /// ////////////////////////////////////////////////////////
        // A I   S T R E S S E D
        /// ////////////////////////////////////////////////////////

        // AI ship is stressed
        this.maneuversWhenStressed = new (Maneuver, int, int)[8][] {
            // NORTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH EAST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // SOUTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
            // NORTH_WEST
            new (Maneuver, int, int)[10] {
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
                maneuvers[0][0],
            },
        };
    }
}
