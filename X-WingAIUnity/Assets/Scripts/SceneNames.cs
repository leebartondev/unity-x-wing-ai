﻿/// ////////////////////////////////////////////////////////
// S CENE NAMES
/// ////////////////////////////////////////////////////////

public class SceneNames {
    public const string AI_SHIP_SELECTION = "AI Ship Selection";
    public const string CLASS_AI = "Dev Classic AI";
    public const string FLYING_SOLO = "Dev Flying Solo";
    public const string MAIN_MENU = "Main Menu";
}
