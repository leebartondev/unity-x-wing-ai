// Ship object example
const X_WING = {
  name: '',
  faction: '',
  maneuvers: {
    blue: [],
    white: [],
    red: []
  },
  actions: ['TARGET_LOCK', 'FOCUS', 'BARREL_ROLL'],

  /**
   * 2D array of possible maneuvers based on enemy's position.
   * Each index of the parent array represents the position
   * of the enemey ship relative to the AI's ship, and the
   * possible appropriate
   */
  enemy_positions: {
    closing: [
      [ BL(1), F(1), F(1), F(1), F(1), F(2), F(2), F(2), K(4), TRR(3) ], // 12 O'clock
      [ F(1), BR(1), BR(1), BR(1), BR(1), BR(2), BR(2), BR(2), TR(2), TR(2) ], // 1-2 O'clock
      [ TR(2), TR(2), TR(2), TR(2), TR(3), TR(3), TR(3), TR(3), K(4), TRR(3) ] // 3 O'clock
      /*...*/
    ],
    away: [/*...*/],
    far: [/*...*/],
    stressed: [/*...*/]
  }
}

// Example Maneuver object
class Maneuver {
  constructor(name, distance) {
      this.name = name;
      this.num = distance;
  }
}

// Example SHIP maneuver method
function F( distance ) {
  return new Maneuver("forward", distance);
}

/**
 * Looks like pick method takes one of enemy positions
 * property of the 'SHIP' object with the direction
 * (0-7) as the index & returns a random Maneuver
 * object.
 *
 * --> pick( SHIP.away[direction] )
 */
function pick( options )
{
	if (options === undefined)
	{
		return invalid();
	}

	var size=options.length;
	if ( size == 0 )
	{
		return invalid();
	}
    var choice=Math.floor(Math.random()*size);
    return options[choice];
}